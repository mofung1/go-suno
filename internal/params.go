package internal

import "time"

// GenerateReq generate-Req
type GenerateReq struct {
	GptDescriptionPrompt string `json:"gpt_description_prompt"`
	Prompt               string `json:"prompt"`
	Mv                   string `json:"mv"`
	Title                string `json:"title"`
	Tags                 string `json:"tags"`
}

// GenerateResp generate-Resp
type GenerateResp struct {
	BatchSize         int       `json:"batch_size"`
	Clips             []Clips   `json:"clips"`
	CreatedAt         time.Time `json:"created_at"`
	ID                string    `json:"id"`
	Status            string    `json:"status"`
	Metadata          `json:"metadata"`
	MajorModelVersion string `json:"major_model_version"`
}

// TokenResp token-Response
type TokenResp struct {
	Jwt    string
	Object string
}

// SidResp session-resp
type SidResp struct {
	Response struct {
		Object              string      `json:"object"`
		ID                  string      `json:"id"`
		Sessions            []Session   `json:"sessions"`
		SignIn              interface{} `json:"sign_in"`
		SignUp              interface{} `json:"sign_up"`
		LastActiveSessionID string      `json:"last_active_session_id"`
		CreatedAt           time.Time   `json:"created_at"`
		UpdatedAt           time.Time   `json:"updated_at"`
	} `json:"response"`
	Client interface{} `json:"client"`
}

// Clips clips
type Clips struct {
	Detail            string      `json:"detail"`
	Id                string      `json:"id"`
	VideoUrl          string      `json:"video_url"`
	AudioUrl          string      `json:"audio_url"`
	ImageUrl          string      `json:"image_url"`
	ImageLargeUrl     string      `json:"image_large_url"`
	MajorModelVersion string      `json:"major_model_version"`
	ModelName         string      `json:"model_name"`
	Metadata          *Metadata   `json:"metadata"`
	IsLiked           bool        `json:"is_liked"`
	UserId            string      `json:"user_id"`
	IsTrashed         bool        `json:"is_trashed"`
	Reaction          interface{} `json:"reaction"`
	CreatedAt         time.Time   `json:"created_at"`
	Status            string      `json:"status"`
	Title             string      `json:"title"`
	PlayCount         int         `json:"play_count"`
	UpvoteCount       int         `json:"upvote_count"`
	IsPublic          bool        `json:"is_public"`
}

// Metadata Metadata
type Metadata struct {
	Tags                 string      `json:"tags"`
	Prompt               string      `json:"prompt"`
	GptDescriptionPrompt string      `json:"gpt_description_prompt"`
	AudioPromptId        interface{} `json:"audio_prompt_id"`
	History              interface{} `json:"history"`
	ConcatHistory        interface{} `json:"concat_history"`
	Type                 string      `json:"type"`
	Duration             float64     `json:"duration"`
	RefundCredits        bool        `json:"refund_credits"`
	Stream               bool        `json:"stream"`
	ErrorType            interface{} `json:"error_type"`
	ErrorMessage         interface{} `json:"error_message"`
}

// Session sessionId
type Session struct {
	Object string `json:"object"`
	ID     string `json:"id"`
}
