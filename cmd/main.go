package main

import (
	"github.com/gin-gonic/gin"
	"go-suno/internal"
	"go-suno/pkg/viper"
)

func main() {
	r := gin.Default()
	// 配置加载
	viper.Init()
	// 使用中间件
	internal.RateLimitMiddleware(r, 1, 100)
	// Suno账号保活
	go internal.SunoService.KeepLive()
	// 创建一个路由组，可以为其添加中间件
	handler := new(internal.Handler)
	v1 := r.Group("/api")
	{
		v1.POST("/gen", handler.GenMusic)
		v1.GET("/feed", handler.GetFeed)
	}
	_ = r.Run()
}
