package autoload

type Config struct {
	*Suno `mapstructure:"suno"`
}

type Suno struct {
	Cookie string `mapstructure:"cookie"`
}
