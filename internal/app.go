package internal

import "go-suno/config/autoload"

var (
	// BasePath 项目根目录
	BasePath string
	// Conf 全局配置
	Conf *autoload.Config
)
