package internal

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct{}

// genReq 请求数据的结构体
type genReq struct {
	Prompt string `json:"prompt"`
}

// feedReq 请求数据的结构体
type feedReq struct {
	ClipsIds []string `json:"clips_ids"`
}

// GenMusic 提交音乐请求
func (h *Handler) GenMusic(c *gin.Context) {
	var genReq genReq
	if err := c.ShouldBindJSON(&genReq); err != nil {
		errResponse(c, err.Error(), "Params Fail")
		return
	}

	res, err := SunoService.Generate(genReq.Prompt)
	if err != nil {
		errResponse(c, err.Error(), "Request Failed")
		return
	}

	okResponse(c, res)
	return
}

// GetFeed 查询详情
func (h *Handler) GetFeed(c *gin.Context) {
	var feedReq feedReq
	if err := c.ShouldBindJSON(&feedReq); err != nil {
		errResponse(c, err.Error(), "Params Fail")
		return
	}

	data, err := SunoService.GetFeed(feedReq.ClipsIds)
	if err != nil {
		errResponse(c, err.Error(), "Request Failed")
		return
	}
	okResponse(c, data)
	return
}

// errResponse 失败请求
func errResponse(c *gin.Context, msg string, errType string) {
	c.JSON(http.StatusBadRequest, gin.H{
		"msg":  msg,
		"type": errType,
	})
}

// okResponse 成功请求
func okResponse(c *gin.Context, data interface{}) {
	c.JSON(http.StatusOK, data)
}
